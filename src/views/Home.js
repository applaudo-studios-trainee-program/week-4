import * as React from 'react';
import Hero from 'components/UI/Hero';

const Home = ({ setPathname }) => {
  return (
    <>
      <Hero setPathname={setPathname} />
    </>
  );
};

export default Home;
