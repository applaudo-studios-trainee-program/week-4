import * as React from 'react';

const NotFound = () => {
  return (
    <div>
      <h1>Path not Found</h1>
    </div>
  );
};

export default NotFound;
