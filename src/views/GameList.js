import * as React from 'react';

import GameCard from 'components/GameCard';
import Button from 'components/Button';
import Styles from './GameList.module.css';
import { getGames } from 'helpers/getGames';
import { usePagination } from 'hooks/usePagination';

const GameList = ({ setPathname }) => {
  const [gameList, setGameList] = React.useState([]);

  React.useEffect(() => setGameList(getGames()), []);

  const { itemsToShow, actualPage, totalPages, setActualPage } = usePagination({
    list: gameList,
    itemsPerPage: 10,
  });

  const handleBackClick = () => setActualPage((state) => state - 1);
  const handleNextClick = () => setActualPage((state) => state + 1);

  return (
    <div className={Styles.container}>
      <section className={Styles['game-list']}>
        {itemsToShow.map((gameData) => (
          <GameCard key={gameData.id} {...gameData} setPathname={setPathname} />
        ))}
      </section>

      <div className={Styles.actions}>
        <Button
          content={'Back'}
          disabled={actualPage === 1 ? true : false}
          onClickFn={handleBackClick}
        />

        <Button
          content={'Next'}
          disabled={actualPage === totalPages ? true : false}
          onClickFn={handleNextClick}
        />
      </div>
    </div>
  );
};

export default GameList;
