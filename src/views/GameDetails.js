import * as React from 'react';

import MetaInfo from 'components/MetaInfo';
import Comment from 'components/Comment';
import Styles from './GameDetails.module.css';

const META_INFO = [
  { id: 0, title: 'Release Date', content: ['Sep 6, 2017'] },
  {
    id: 1,
    title: 'Developers',
    content: ['Bungie, Inc.', 'Vicarious Visions'],
  },
  {
    id: 2,
    title: 'Publisher',
    content: ['Activision Blizzard', 'Bungie'],
  },
  {
    id: 3,
    title: 'Genres',
    content: ['Action', 'Shooter', 'Massively Multiplayer'],
  },
];

const COMMENTS = [
  {
    id: 0,
    author: 'Bryan Mendoza',
    content:
      'Quisque porta tristique augue, in fermentum quam viverra quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce posuere, nisi eget ornare accumsan, mi elit.',
  },
  {
    id: 1,
    author: 'Robbie Ramirez',
    content:
      'In vel maximus libero. Aenean sed ipsum non libero eleifend molestie. Duis orci libero, condimentum eget volutpat eget, sodales vitae ante. Suspendisse ac fermentum nisl. Nunc vitae libero vel urna.',
  },
  {
    id: 2,
    author: 'Firat Hendrix',
    content:
      'Aliquam erat volutpat. Donec suscipit magna ac augue lacinia, id finibus mauris rhoncus. Praesent cursus hendrerit sapien non mollis. Lorem ipsum dolor sit amet, consectetur.',
  },
  {
    id: 3,
    author: 'Edward Thorpe',
    content:
      'In porttitor, enim et sagittis ultricies, arcu nulla fermentum erat, quis feugiat sem sapien at dui. Vestibulum porttitor sapien convallis, rhoncus enim nec, ultricies dolor. Vestibulum feugiat, mauris vitae vehicula.',
  },
  {
    id: 4,
    author: 'Aliesha Gale',
    content:
      'Donec laoreet suscipit mi, at pretium lacus convallis nec. Integer commodo ultrices felis iaculis maximus. Nullam tempor elementum quam. Nam.',
  },
];

const GameDetails = ({ params }) => {
  return (
    <div className={Styles.container}>
      <h1 className={Styles.title}>Destiny 2</h1>

      <img
        className={Styles.image}
        src="https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        alt="Destiny 2 Cover"
        title="Destiny 2 Cover"
      />

      <section className={Styles.description}>
        <h2 className={Styles['description-title']}>About</h2>
        <p className={Styles['description-content']}>
          Destiny 2 is an online multiplayer first-person shooter. You take on
          the role of a Guardian that needs to protect the last city on Earth
          from alien invaders. The game follows its predecessor, Destiny. The
          goal of the game is to return the Light that was stolen from the
          Guardians by the aliens. Destiny 2 features two main activity types:
          player versus environment and player versus player.
        </p>
      </section>

      <section className={Styles['meta-container']}>
        {META_INFO.map((data) => (
          <MetaInfo key={data.id} {...data} />
        ))}
      </section>

      <section className={Styles['comment-list']}>
        <h2 className={Styles['comment-list-title']}>Comments</h2>

        {COMMENTS.map((comment) => (
          <Comment key={comment.id} {...comment} />
        ))}
      </section>
    </div>
  );
};

export default GameDetails;
