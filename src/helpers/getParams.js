import { pathToREGEX } from './pathToREGEX';

export const getParams = (matchedRoute, pathname) => {
  const paramList =
    pathname
      .match(pathToREGEX(matchedRoute.path))
      .filter((_, index) => index > 0) || [];

  const keys = Array.from(matchedRoute.path.matchAll(/:(\w+)/g)).map(
    (urlParam) => urlParam[1]
  );

  return Object.fromEntries(
    keys.map((param, index) => [param, paramList[index]])
  );
};
