import * as React from 'react';

import Navbar from 'components/UI/Navbar';
import Footer from 'components/UI/Footer';
import Home from 'views/Home';
import GameList from 'views/GameList';
import GameDetails from 'views/GameDetails';
import NotFound from 'views/NotFound';
import { pathToREGEX } from 'helpers/pathToREGEX';
import { getParams } from 'helpers/getParams';

const ROUTE_LIST = [
  { path: '/', view: 'Home' },
  { path: '/game-list', view: 'GameList' },
  { path: '/game-details/:id', view: 'GameDetails' },
];

const Router = () => {
  const historyAPIRef = React.useRef(window.history);

  const [view, setView] = React.useState('Home');
  const [params, setParams] = React.useState({});
  const [pathname, setPathname] = React.useState(window.location.pathname);

  React.useEffect(() => {
    const matchedRoute = ROUTE_LIST.find((route) =>
      pathToREGEX(route.path).test(pathname)
    ) || { path: '/not-found', view: 'NotFound' };

    if (matchedRoute.path !== '/not-found') {
      const params = getParams(matchedRoute, pathname);

      setParams(params);
      setView(matchedRoute.view);
    } else {
      setParams({});
      setView(matchedRoute.view);
    }

    historyAPIRef.current.pushState(null, '', pathname);
  }, [pathname]);

  return (
    <>
      <header>
        <Navbar setPathname={setPathname} />
      </header>

      <main>
        {view === 'Home' ? (
          <Home setPathname={setPathname} />
        ) : view === 'GameList' ? (
          <GameList setPathname={setPathname} />
        ) : view === 'GameDetails' ? (
          <GameDetails params={params} />
        ) : (
          <NotFound />
        )}
      </main>

      <Footer setPathname={setPathname} />
    </>
  );
};

export default Router;
