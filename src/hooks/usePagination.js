import { useEffect, useState } from 'react';

export const usePagination = ({ list, itemsPerPage }) => {
  const [itemsToShow, setItemsToShow] = useState([]);
  const [actualPage, setActualPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => setTotalPages(Math.ceil(list.length / itemsPerPage)), [
    list,
    itemsPerPage,
  ]);

  useEffect(() => {
    const itemsToShow = list.filter(
      (_, index) =>
        index >= (actualPage - 1) * itemsPerPage &&
        index <= itemsPerPage * actualPage - 1
    );

    setItemsToShow(itemsToShow);
  }, [actualPage, list, itemsPerPage]);

  return { itemsToShow, actualPage, totalPages, setActualPage };
};
