import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Router from 'Router';
import 'index.css';

ReactDOM.render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>,
  document.getElementById('root')
);
