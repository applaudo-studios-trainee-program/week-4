const games = [
  {
    id: 3498,
    backgroundImage:
      'https://media.rawg.io/media/games/84d/84da2ac3fdfc6507807a1808595afb12.jpg',
    name: 'Grand Theft Auto V',
  },
  {
    id: 4200,
    backgroundImage:
      'https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg',
    name: 'Portal 2',
  },
  {
    id: 3328,
    backgroundImage:
      'https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg',
    name: 'The Witcher 3: Wild Hunt',
  },
  {
    id: 5286,
    backgroundImage:
      'https://media.rawg.io/media/games/ad2/ad2ffdf80ba993654f31da045bc02456.jpg',
    name: 'Tomb Raider (2013)',
  },
  {
    id: 5679,
    backgroundImage:
      'https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg',
    name: 'The Elder Scrolls V: Skyrim',
  },
  {
    id: 12020,
    backgroundImage:
      'https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg',
    name: 'Left 4 Dead 2',
  },
  {
    id: 802,
    backgroundImage:
      'https://media.rawg.io/media/games/588/588c6bdff3d4baf66ec36b1c05b793bf.jpg',
    name: 'Borderlands 2',
  },
  {
    id: 4062,
    backgroundImage:
      'https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg',
    name: 'BioShock Infinite',
  },
  {
    id: 13536,
    backgroundImage:
      'https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg',
    name: 'Portal',
  },
  {
    id: 3439,
    backgroundImage:
      'https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg',
    name: 'Life is Strange',
  },
  {
    id: 4291,
    backgroundImage:
      'https://media.rawg.io/media/games/736/73619bd336c894d6941d926bfd563946.jpg',
    name: 'Counter-Strike: Global Offensive',
  },
  {
    id: 1030,
    backgroundImage:
      'https://media.rawg.io/media/games/929/9295e55ce69cf5337c567983cf8b4137.jpeg',
    name: 'Limbo',
  },
  {
    id: 4286,
    backgroundImage:
      'https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg',
    name: 'BioShock',
  },
  {
    id: 13537,
    backgroundImage:
      'https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg',
    name: 'Half-Life 2',
  },
  {
    id: 28,
    backgroundImage:
      'https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg',
    name: 'Red Dead Redemption 2',
  },
  {
    id: 11859,
    backgroundImage:
      'https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg',
    name: 'Team Fortress 2',
  },
  {
    id: 2454,
    backgroundImage:
      'https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg',
    name: 'DOOM (2016)',
  },
  {
    id: 3070,
    backgroundImage:
      'https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg',
    name: 'Fallout 4',
  },
  {
    id: 3939,
    backgroundImage:
      'https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg',
    name: 'PAYDAY 2',
  },
  {
    id: 4459,
    backgroundImage:
      'https://media.rawg.io/media/games/4a0/4a0a1316102366260e6f38fd2a9cfdce.jpg',
    name: 'Grand Theft Auto IV',
  },
  {
    id: 3272,
    backgroundImage:
      'https://media.rawg.io/media/games/88c/88c5b4d7c80276c03ff62aebb1a99ad4.jpg',
    name: 'Rocket League',
  },
  {
    id: 58175,
    backgroundImage:
      'https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg',
    name: 'God of War',
  },
  {
    id: 32,
    backgroundImage:
      'https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg',
    name: 'Destiny 2',
  },
  {
    id: 10213,
    backgroundImage:
      'https://media.rawg.io/media/games/83f/83f6f70a7c1b86cd2637b029d8b42caa.jpg',
    name: 'Dota 2',
  },
  {
    id: 23027,
    backgroundImage:
      'https://media.rawg.io/media/games/8d6/8d69eb6c32ed6acfd75f82d532144993.jpg',
    name: 'The Walking Dead: Season 1',
  },
  {
    id: 3192,
    backgroundImage:
      'https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg',
    name: 'Metal Gear Solid V: The Phantom Pain',
  },
  {
    id: 278,
    backgroundImage:
      'https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg',
    name: 'Horizon Zero Dawn',
  },
  //   {
  //     id: 766,
  //     backgroundImage:
  //       'https://media.rawg.io/media/games/f87/f87457e8347484033cb34cde6101d08d.jpg',
  //     name: 'Warframe',
  //   },
  //   {
  //     id: 19103,
  //     backgroundImage:
  //       'https://media.rawg.io/media/games/b7b/b7b8381707152afc7d91f5d95de70e39.jpg',
  //     name: 'Half-Life 2: Lost Coast',
  //   },
];

export default games;
