import * as React from 'react';
import User from './Icons/User';
import Styles from './Comment.module.css';

const Comment = ({ author, content }) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.data}>
        <User className={Styles['data-icon']} />
        <p className={Styles['data-author']}>{author}</p>
      </div>

      <p className={Styles.content}>{content}</p>
    </div>
  );
};

export default Comment;
