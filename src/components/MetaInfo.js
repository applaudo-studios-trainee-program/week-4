import * as React from 'react';

import Styles from './MetaInfo.module.css';

const MetaInfo = ({ title, content }) => {
  return (
    <div className={Styles.container}>
      <h4 className={Styles.title}>{title}</h4>

      <p className={Styles.content}>
        {content.map((content, index) =>
          index === 0 ? content : `, ${content}`
        )}
      </p>
    </div>
  );
};

export default MetaInfo;
