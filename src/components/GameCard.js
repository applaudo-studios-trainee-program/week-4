import * as React from 'react';

import Styles from './GameCard.module.css';

const GameCard = ({ backgroundImage, name, setPathname }) => {
  const handleAnchorClick = (event) => {
    event.preventDefault();

    setPathname(event.currentTarget.pathname);
  };

  return (
    <a
      className={Styles.link}
      href="/game-details/gge5"
      onClick={handleAnchorClick}
    >
      <div className={Styles.container}>
        <img
          className={Styles.image}
          src={backgroundImage}
          alt={`${name} Cover`}
          title={`${name} Cover`}
        />

        <p className={Styles.name}>{name}</p>
      </div>
    </a>
  );
};

export default GameCard;
