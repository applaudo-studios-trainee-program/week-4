import * as React from 'react';

import Styles from './Hero.module.css';

const Hero = ({ setPathname }) => {
  const handleAnchorClick = (event) => {
    event.preventDefault();

    setPathname(event.target.pathname);
  };

  return (
    <section className={Styles.container}>
      <h1 className={Styles.title}>React Stack</h1>
      <p className={Styles['sub-title']}>Your realiable game source</p>

      <a className={Styles.link} href="/game-list" onClick={handleAnchorClick}>
        Full Game List
      </a>
    </section>
  );
};

export default Hero;
