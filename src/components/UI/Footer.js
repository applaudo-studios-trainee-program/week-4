import * as React from 'react';
import Styles from './Footer.module.css';
import Navbar from './Navbar';

const Footer = ({ setPathname }) => {
  return (
    <footer className={Styles.container}>
      <h3 className={Styles.title}>React Stack</h3>

      <Navbar setPathname={setPathname} />
    </footer>
  );
};

export default Footer;
