import * as React from 'react';
import Styles from './Navbar.module.css';

const Navbar = ({ setPathname }) => {
  const handleAnchorClick = (event) => {
    event.preventDefault();

    setPathname(event.target.pathname);
  };

  return (
    <nav className={Styles.container}>
      <ul className={Styles['item-list']}>
        <li className={Styles['list-item']}>
          <a
            className={Styles['list-link']}
            href="/"
            onClick={handleAnchorClick}
          >
            Home
          </a>
        </li>
        <li className={Styles['list-item']}>
          <a
            className={Styles['list-link']}
            href="/game-list"
            onClick={handleAnchorClick}
          >
            Game List
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default React.memo(Navbar);
