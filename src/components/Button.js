import * as React from 'react';
import Styles from './Button.module.css';

const Button = ({ content, disabled, onClickFn }) => {
  return (
    <button className={Styles.button} disabled={disabled} onClick={onClickFn}>
      {content}
    </button>
  );
};

export default Button;
